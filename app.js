'use strict'

const Discord = require("discord.js");

const data = require("./config.json");
const prefix = data.prefix;
const token = data.botToken;

const client = new Discord.Client();

var startTime = Date.now();
var currentTime = new Date().toLocaleTimeString();

client.on("ready", () => {
  console.log(`[ BOT ] : ${currentTime} : Logged in as ${client.user.tag}, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds!`);
  console.log('------');

  client.guilds.forEach(function(item) {
    var serverId = item.id;
    var serverObj = client.guilds.get(serverId);
    var botRole = serverObj.roles.find(x => x.name === "Rukia");
    //botRole.setColor('#12ba58');
  });

  function botPresence(client) {
    client.user.setPresence({ game: { name: "Botting || -help", type: 0 } });
    setTimeout(function(){
      client.user.setPresence({ game: { name: `${client.users.size} Users || -help`, type: 0 }});
    }, 10000);
    setTimeout(function(){
      client.user.setPresence({ game: { name: `${client.guilds.size} Guilds || -help`, type: 0 }});
    }, 20000);
    setTimeout(function(){
      var time = new Date().toLocaleTimeString();
      client.user.setPresence({ game: { name: `${time} || -help`, type: 0 }});
    }, 30000);
  }

  botPresence(client);
  setInterval(function() { botPresence(client); }, 40000);
});

client.on('guildMemberAdd', member => {
  const channel = member.guild.channels.find('name', 'general');
  if (!channel) return;
  channel.send(`Welcome to the server, ${member}!`);
  member.addRole('Verified User').catch(console.error);
});

client.on("message", async message => {
  if(message.author.bot) return;
  if(message.content.indexOf(data.prefix) !== 0) return;
  const args = message.content.slice(data.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  // Start General Usage Commands
  if(command === "help") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Mod", "Mods", "Moderator", "Moderators"].includes(r.name)) )
      return message.reply({"embed": {
          "title": "Command Listing for Rukia",
          "description": "Here you can see the commands that you have access to with Rukia!",
          "footer": { "text": "Have Issues or Concerns? Please use the -report command!" },
          "color": Math.floor(Math.random() * 16777214) + 1,
          "author": {"name": "Rukia","icon_url": client.user.displayAvatarURL},
          "fields": [
            {"name": "General","value": "-help - shows this message.\n-info - shows information about this bot.\n-report - report an issue with bot.\n-feedback - give feedback about the bot."},
          ]
        }}
      );
      if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators"].includes(r.name)) )
        return message.reply({"embed": {
          "title": "Moderator Command Listing for Rukia",
          "description": "Here you can see the commands that you have access to with Rukia!",
          "footer": { "text": "Have Issues or Concerns? Please use the -report command!" },
          "color": Math.floor(Math.random() * 16777214) + 1,
          "author": {"name": "Rukia","icon_url": client.user.displayAvatarURL},
          "fields": [
            {"name": "General","value": "-help - shows this message.\n-info - shows information about this bot.\n-report - report an issue with bot.\n-feedback - give feedback about the bot."},
            {"name": "Moderators","value": "-userinfo USER - shows info on mentioned user.\n-kick USER REASON - kicks the mentioned user."}
          ]
        }});
    message.reply({"embed": {
      "title": "Admin Command Listing for Rukia",
      "description": "Here you can see the commands that you have access to with Rukia!",
      "footer": { "text": "Have Issues or Concerns? Please use the -report command!" },
      "color": Math.floor(Math.random() * 16777214) + 1,
      "author": {"name": "Rukia","icon_url": client.user.displayAvatarURL},
      "fields": [
        {"name": "General","value": "-help - shows this message.\n-info - shows information about this bot.\n-report - report an issue with bot.\n-feedback - give feedback about the bot."},
        {"name": "Moderators","value": "-kick USER REASON - kicks the mentioned user.\n-userinfo USER - shows info on mentioned user."},
        {"name": "Administrators","value": "-ping - shows the latency of the API and Server.\n-uptime - shows the uptime of the bot.\n-ban USER REASON - bans the mentioned user.\n-bans - lists the banned users of the server."}
      ]
    }});
  }

  if(command === "info") {
    var user = client.user;
    var em = new Discord.RichEmbed();
    em.setTitle("Switch's AIO Discord Bot")
    em.setColor("RANDOM")
    em.setThumbnail(user.displayAvatarURL)
    em.setDescription("The official AIO bot from Switch!")
    em.setFooter("Want to add me to your own server? Please use the -invite command.")
    em.addField("Owner","<@213488164826906624>",true)
    em.addField("Author","<@213488164826906624>",true)
    em.addField("Bot ID",user.id)
    em.addField("Have Suggestions or Feedback?","Please use the -feedback command!",true)
    em.addField("Have Issues or Concerns?","Please use the -report command!",true);
    message.channel.send(em);
  }

  if(command === "report") {
    client.users.get("213488164826906624").send({embed: {
      color: 13632027,
      "title": `Report from ${message.member.displayName}`,
      "description":`<@${message.member.id}> ${message.content}`,
    }});
  }

  if(command === "feedback") {
    client.users.get("213488164826906624").send({embed: {
      color: 4289797,
      "title": `Feedback from ${message.member.displayName}`,
      "description":`<@${message.member.id}> ${message.content}`,
    }});
  }
  // End General Usage Commands

  // Start Mod Commands
  if(command === "userinfo") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Mod", "Mods", "Moderator", "Moderators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    var member = message.mentions.members.first();
    var userRoles = [];
    member._roles.forEach(function(item) {
      let myRole = message.guild.roles.get(item).name;
      userRoles.push(myRole);
    });
    message.channel.send({embed: {
      "title":`${member.displayName}'s Information`,
      "footer": `Requested by ${message.member.displayName}.`,
      "fields": [
        {"name":"Username","value":member.user.username + "#" + member.user.discriminator,"inline": true},
        {"name":"Nickname","value":member.nickname,"inline": true},
        {"name":"User ID","value":member.id,"inline": true},
        {"name":"Joined At","value":member.joinedAt,"inline": true},
        {"name":"Roles","value":userRoles.toString()}
      ]
    }});
  }

  if(command === "kick") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Mod", "Mods", "Moderator", "Moderators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.kickable)
      return message.reply("I cannot kick this user! Do they have a higher role? Do I have kick permissions?");
    let reason = args.slice(1).join(' ');
    if(!reason)
      return message.reply("Please indicate a reason for the kick!");
    await member.kick(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));
    message.reply(`${member.user.tag} has been kicked by ${message.author.tag} because: ${reason}`);
  }
  // End Mod Commands

  // Start Dev Commands
  if(command === "ping") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
		var m = await message.channel.send("Ping?");
			m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`)
	}

  if(command === "uptime") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    var now = Date.now();
  	var msec = now - startTime;
  	var days = Math.floor(msec / 1000 / 60 / 60 / 24);
  	msec -= days * 1000 * 60 * 60 * 24;
  	var hours = Math.floor(msec / 1000 / 60 / 60);
  	msec -= hours * 1000 * 60 * 60;
  	var mins = Math.floor(msec / 1000 / 60);
  	msec -= mins * 1000 * 60;
  	var secs = Math.floor(msec / 1000);
  	var timestr = "";
  	if(days > 0) { timestr += days + " days "; }
  	if(hours > 0) { timestr += hours + " hours "; }
  	if(mins > 0) { timestr += mins + " minutes "; }
  	if(secs > 0) { timestr += secs + " seconds ";	}
  	("**Uptime**: " + timestr);
  }

  if(command === "test") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    var curServerName = message.guild.owner.user.username + "#" + message.guild.owner.user.discriminator;
    var curServerId = message.guild.owner.user.id;
    var star = client.emojis.find(x => x.name === "star");
    var entries = [];
    client.guilds.forEach(function(item) {
      var serverId = item.id;
      var serverName = client.guilds.get(serverId).name;
      var ownerName = client.guilds.get(serverId).owner.user.username + "#" + client.guilds.get(serverId).owner.user.discriminator;
      var ownerId = client.guilds.get(serverId).owner.user.id;
      entries.push({
        name: serverName + " - Id: " + serverId,
        value: ownerName + " - Id: " + ownerId
      });
    });
    message.reply("", {
      embed: {
          title: "Server Owner Details",
          color: Math.floor(Math.random() * 16777214) + 1,
          fields: entries,
          footer: { "text": "Have Issues or Concerns? Please use the -report command!" },
      },
    });
  }
  // End Dev commands

  // Start Admin Commands
  if(command === "ban") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.bannable)
      return message.reply("I cannot ban this user!");
    let reason = args.slice(1).join(' ');
    if(!reason)
      return message.reply("Please indicate a reason for the ban!");
    await member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));
    message.reply(`${member.user.tag} has been banned by ${message.author.tag} because: ${reason}`);
  }

  if(command === "bans") {
    if(!message.member.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators", "Bot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
  	message.guild.fetchBans().then((users) => {
  		if(users.size == 0){
  			message.channel.send("No one has been banned from this server!");
  		} else if (users.size != null){
  			var userArray = users.array();
        if(users.size == 1) {
          var response = "This guild has " + users.size + " ban.";
        } else if(users.size >= 2) {
          var response = "This guild has " + users.size + " bans.";
        }
  			for(var user in userArray){
  				response += "\nBanned users:\n User: " + userArray[user].username + ' - ID: ' + userArray[user].id;
  			}
  			message.channel.send(response);
  		}
  	});
  }
  // End Admin Commands

  // Start Server Owner Commands
  if (command === 'setup') {
    if (message.author.id !== message.guild.owner.user.id)
      return message.reply("Sorry, you don't have permissions to use this!");
    let genchannel = message.guild.channels.find(x => x.name === "general");
    if(!genchannel)
      message.guild.createChannel("general", "text")
        .then(m => {
          m.overwritePermissions(message.guild.id, {
            VIEW_CHANNEL: true
          })
        })
    let modchannel = message.guild.channels.find(x => x.name === "mod-log");
    if(!modchannel)
      message.guild.createChannel("mod-log", "text")
        .then(m => {
          m.overwritePermissions(message.guild.id, {
            VIEW_CHANNEL: false
          })
          m.overwritePermissions(message.author.id, {
            VIEW_CHANNEL: true
          })
        })
    let botrole = message.guild.roles.find(x => x.name === "Bot");
    if(!botrole)
      message.guild.createRole({
        data: {
          name: 'Bot',
          color: '#1f8b4c',
          permissions: ['ADMINISTRATOR']
        },
        reason: 'First Time Setup'
      })
    let modrole = message.guild.roles.some(r=>["Mod", "Mods", "Moderator", "Moderators"].includes(r.name));
    if(!modrole)
      message.guild.createRole({
        data: {
          name: 'Bot',
          color: '#1f8b4c',
          permissions: ['ADMINISTRATOR']
        },
        reason: 'First Time Setup'
      })
    let adminrole = message.guild.roles.some(r=>["Admin", "Admins", "Administrator", "Administrators"].includes(r.name))
    if(!adminrole)
      message.guild.createRole({
        data: {
          name: 'Bot',
          color: '#1f8b4c',
          permissions: ['ADMINISTRATOR']
        },
        reason: 'First Time Setup'
      })
    let vusrrole = message.guild.roles.find(x => x.name === "Verified User");
    if(!vusrrole)
      message.guild.createRole({
        data: {
          name: 'Verified User',
          color: '#f1c40f',
          permissions: ['CREATE_INSTANT_INVITE', 'CHANGE_NICKNAME', 'SEND_MESSAGES', 'EMBED_LINKS', 'READ_MESSAGE_HISTORY', 'MENTION_EVERYONE', 'ADD_REACTIONS', 'CONNECT', 'SPEAK', 'USE_VAD']
        },
        reason: 'First Time Setup'
      })

  }
  // End Bot Server Commands

  // Start Bot Owner Commands
  if (command === 'botannounce') {
    if (message.author.id !== '213488164826906624')
      return message.reply("Sorry, you don't have permissions to use this!");
    client.guilds.forEach(function(item) {
      console.log(item.id);
      var guild = item.id;
      //var channel = guild.channels.find("name", "general");
    });
  }

  if (command === 'botstats') {
    if (message.author.id !== '213488164826906624')
      return message.reply("Sorry, you don't have permissions to use this!");
    return client.shard.broadcastEval('this.guilds.size')
      .then(results => {
        return message.channel.send(`Server count: ${results.reduce((prev, val) => prev + val, 0)}`);
      })
      .catch(console.error);
  }

  if(command === "botowners") {
    if (message.author.id !== '213488164826906624')
      return message.reply("Sorry, you don't have permissions to use this!");
    var curServerName = message.guild.owner.user.username + "#" + message.guild.owner.user.discriminator;
    var curServerId = message.guild.owner.user.id;
    var star = client.emojis.find(x => x.name === "star");
    var entries = [];
    client.guilds.forEach(function(item) {
      var serverId = item.id;
      var serverName = client.guilds.get(serverId).name;
      var ownerName = client.guilds.get(serverId).owner.user.username + "#" + client.guilds.get(serverId).owner.user.discriminator;
      var ownerId = client.guilds.get(serverId).owner.user.id;
      entries.push({
        name: serverName + " - Id: " + serverId,
        value: ownerName + " - Id: " + ownerId
      });
    });
    message.reply("", {
      embed: {
          title: "Server Owner Details",
          color: Math.floor(Math.random() * 16777214) + 1,
          fields: entries,
          footer: { "text": "Have Issues or Concerns? Please use the -report command!" },
      },
    });
  }
  // End Bot Owner Commands
});

client.login(token);
