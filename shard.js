const data = require('./config.json')

const { ShardingManager } = require('discord.js');
const shard = new ShardingManager('./app.js', { token: data.token });

var currentTime = new Date().toLocaleTimeString();

shard.spawn();

shard.on('launch', shard => console.log(`[SHARD] : ${currentTime} : New shard id:  ${shard.id}\n[SHARD] : ${currentTime} : Active shards: ${shard.manager.totalShards}`));
